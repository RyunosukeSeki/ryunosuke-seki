﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class needle : MonoBehaviour
{
    //unityから値の変更を可能に
    [SerializeField]
    //移動速度
    float speed = -100f;

    //物理挙動のコンポーネント
    Rigidbody2D rigidbody_;

    //初期化
    void Start()
    {
        //物理挙動のコンポーネントを取得
        rigidbody_ = GetComponent<Rigidbody2D>();

        //プレイヤーの方向へ移動
        rigidbody_.AddForce(new Vector2(speed, 0));
    }

    //更新
    void Update()
    {
        //座標を取得
        Vector2 position = transform.position;
        //画面左端に来たら
        if(position.x < GetLeft())
        {
            //針を消す
            Destroy(gameObject);
        }
    }

    //画面左端の座標を取得
    float GetLeft()
    {
        Vector2 left_coordinate = Camera.main.ViewportToWorldPoint(Vector2.zero);
        return left_coordinate.x;
    }

    //速度設定
    public void SetSpeed(float sp)
    {
        speed = sp;
    }
}
