﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class needleManager : MonoBehaviour
{
    //オブジェクトを生成
    public GameObject needle;
    //経過時間用の変数
    float totalTime = 0f;
    //タイマー用の変数
    float timer = 0f;
    //針の生成数
    int count = 0;

    //初期化
    void Start()
    {
        
    }

    //更新
    void Update()
    {
        //経過時間を計測
        totalTime += Time.deltaTime;

        //一定時間経つと
        timer -= Time.deltaTime;
        if(timer < 0)
        {
            //障害物を初期位置からランダムな高さに生成
            Vector3 position = transform.position;
            position.y = Random.Range(-5, 5);
            GameObject nextNeedle = Instantiate(needle, position, Quaternion.identity);

            //needleスクリプトを取得
            needle needleScript = nextNeedle.GetComponent<needle>();

            //デフォルトの速度を設定し、一秒ごとに速度を10上げる
            float speed = 100 + (totalTime * 10);
            //値を渡す
            needleScript.SetSpeed(-speed);

            

            //生成数をカウント
            count++;
            //針の生成タイミングをずらす
            if(count%5 < 3)
            {
                //0.1秒後に再生成
                timer += 0.1f;
            }else{
                //1秒後に再生成
                timer += 1;
            }
        }
    }
}
