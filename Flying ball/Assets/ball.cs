﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ball : MonoBehaviour
{
    //プレイヤーのジャンプ力
    float JumpPower = 600f;

    //物理挙動のコンポーネント
    Rigidbody2D rigidbody;

    //初期化
    void Start()
    {
        //物理挙動のコンポーネントを取得
        rigidbody = GetComponent<Rigidbody2D>();
    }

    //更新
    void Update()
    {
        //座標を取得
        Vector3 position = transform.position;
        float y = transform.position.y;

        //画面下に行ったとき
        if (y < GetBottom())
        {
            //移動速度を0にする
            rigidbody.velocity = Vector2.zero;
            //ジャンプする(上方向へ移動)
            rigidbody.AddForce(new Vector2(0, JumpPower));
            //プレイヤーの位置を画面内に戻す
            position.y = GetBottom();
        }

        //変更した座標を反映
        transform.position = position;
    }

    //画面下の座標を取得
    float GetBottom()
    {
        Vector2 left_coordinate = Camera.main.ViewportToWorldPoint(Vector2.zero);
        return left_coordinate.y;
    }
}
