﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour
{
    //unityから値の変更を可能に
    [SerializeField]
    float JumpPower = 100f; //プレイヤーのジャンプ力
    public GameObject game; //オブジェクトを生成

    //物理挙動のコンポーネント
    Rigidbody2D rigidbody;
    //ゲームを管理するためのスクリプト
    GameManager gameManager;

    //初期化
    void Start()
    {
        //物理挙動のコンポーネントを取得
        rigidbody = GetComponent<Rigidbody2D>();
        //ゲームを管理するスクリプトを取得
        gameManager = game.GetComponent<GameManager>();
    }

    //更新
    void Update()
    {
        //スペースキーが入力されたら
        if (Input.GetKeyDown(KeyCode.Space))
        {
            //移動速度を0にする
            rigidbody.velocity = Vector2.zero;
            //ジャンプする(上方向へ移動)
            rigidbody.AddForce(new Vector2(0, JumpPower));
        }

        //座標を取得
        Vector3 position = transform.position;
        float y = transform.position.y;

        //画面上に行ったとき
        if(y > GetTop())
        {
            //移動速度を0にする
            rigidbody.velocity = Vector2.zero;
            //プレイヤーの位置を画面内に戻す
            position.y = GetTop();
        }

        //画面下に行ったとき
        if(y < GetBottom())
        {
            //移動速度を0にする
            rigidbody.velocity = Vector2.zero;
            //ジャンプする(上方向へ移動)
            rigidbody.AddForce(new Vector2(0, JumpPower));
            //プレイヤーの位置を画面内に戻す
            position.y = GetBottom();
        }

        //変更した座標を反映
        transform.position = position;
    }

    //画面上の座標を取得
    float GetTop()
    {
        Vector2 right_coordinate = Camera.main.ViewportToWorldPoint(Vector2.one);
        return right_coordinate.y;
    }

    //画面下の座標を取得
    float GetBottom()
    {
        Vector2 left_coordinate = Camera.main.ViewportToWorldPoint(Vector2.zero);
        return left_coordinate.y;
    }

    //衝突判定
    void OnTriggerEnter2D(Collider2D other)
    {
        Debug.Log("Hit");
        //プレイヤーを削除
        Destroy(gameObject);
        //ゲームオーバーを通知
        gameManager.StartGameOver();
    }
}
