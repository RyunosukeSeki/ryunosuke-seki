﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ScoreManger : MonoBehaviour
{
    //オブジェクトの生成
    public GameObject scoreObject;
    int score = 0;

    //初期化
    void Start()
    {
        //保存されている値を読み込み
        int resultScore = PlayerPrefs.GetInt("SCORE");
        score = resultScore;
    }

    //更新
    void Update()
    {
        //Textのコンポーネントを取得
        Text scoreText = scoreObject.GetComponent<Text>();
        //表示するテキストを設定
        scoreText.text = "Score:" + score;
    }
}
