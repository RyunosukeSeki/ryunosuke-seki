﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    //オブジェクトを生成
    public GameObject scoreObject;
    //スコア用の変数
    public int score = 0;

    //状態定数
    enum State
    {
        Main,       //ゲーム中
        GameOver,   //ゲームオーバー
    }

    //初期の状態(ゲーム中)
    State state = State.Main;

    //ゲームオーバーシーンを開始
    public void StartGameOver()
    {
        state = State.GameOver;
    }

    //初期化
    void Start()
    {
        
    }

    //更新
    void Update()
    {
        //Textのコンポーネントを取得
        Text scoreText = scoreObject.GetComponent<Text>();
        //表示するテキストを設定
        scoreText.text = "Score:" + score;

        //ゲーム進行中のみスコアを加算
        if(state == State.Main)
        {
            //スコアを加算
            score += 1;
        }

        //ステータスがGameOverになったら
        if(state == State.GameOver)
        {
            //scoreの値を保存
            PlayerPrefs.SetInt("SCORE", score);
            PlayerPrefs.Save();
            
            //リザルトシーンへ移行
            Debug.Log("ゲーム終了");
            SceneManager.LoadScene("ResultScene");
        }
    }


}
